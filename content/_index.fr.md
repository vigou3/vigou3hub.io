---
title: Vincent Goulet
description: "Professeur de sciences actuarielles. Geek de placard."
---

Cette page dresse la liste de mes [ressources éducatives libres](https://fr.wikipedia.org/wiki/Ressources_éducatives_libres), logiciels, conférences et ateliers. Tout le matériel est distribué sous [licence libre](https://fr.wikipedia.org/wiki/Licence_libre). Le code source de ces projets est hébergé dans [GitLab](https://gitlab.com/vigou3).

# Ressources éducatives libres

## R et programmation

[**Programmer avec R**](https://vigou3.gitlab.io/programmer-avec-r/) 
Document de référence proposant une introduction à la programmation informatique avec le langage [R](https://www.r-project.org).

[**Présentation de R --- Code réel, objets virtuels**](https://vigou3.gitlab.io/presentation-r/) 
Formation interactive de prise en main de R et d'initiation à la stratégie de travail dite [«le code est réel, les objets sont virtuels»](http://ess.r-project.org/Manual/ess.html#Philosophies-for-using-ESS_0028R_0029).

[**Conception de paquetages R**](https://vigou3.gitlab.io/conception-paquetages-r) 
Formation interactive sur la conception de paquetages [R](https://www.r-project.org).

[**Programmation lettrée et R Markdown**](https://vigou3.gitlab.io/programmation-lettree-et-rmarkdown)
Formation interactive sur la programmation lettrée en général et [R Markdown](https://rmarkdown.rstudio.com) en particulier. 

[**Rapports dynamiques avec Shiny**](https://vigou3.gitlab.io/rapports-dynamiques-avec-shiny) 
Formation interactive sur la production de rapports dynamiques dans R avec la technologie [Shiny](https://shiny.posit.co). 

[**Utilisation et conception d'interfaces API**](https://vigou3.gitlab.io/utilisation-et-conception-interfaces-api) 
Formation interactive sur l'utilisation d'interfaces API depuis la ligne de commande Unix avec curl et la conception dans R avec [**plumber**](https://rplumber.io).

[**Ligne de commande Unix**](https://vigou3.gitlab.io/ligne-commande-unix/) 
Formation interactive d'introduction à l'utilisation de la ligne de commande Unix (notamment [Bash](https://www.gnu.org/software/bash/)).

[**Gestion de versions avec Git**](https://vigou3.gitlab.io/gestion-versions-avec-git/) 
Formation interactive d'introduction à l'utilisation du système de gestion de versions avec [Git](https://git-scm.com).
 
[**Calcul numériques de primes bayésiennes avec Stan**](https://vigou3.gitlab.io/calcul-primes-bayesiennes-avec-stan/) 
Formation interactive d’introduction à l’utilisation de la plateforme de modélisation statistique [Stan](https://mc-stan.org/) dans un contexte bien précis: le calcul numérique de primes bayésiennes en théorie de la crédibilité. 

## Sciences actuarielles

[**Méthodes numériques en actuariat avec R**](https://vigou3.gitlab.io/methodes-numeriques-en-actuariat-avec-r/)
Ouvrage de référence sur les méthodes numériques couvrant la simulation de nombres aléatoires, l'arithmétique des ordinateurs et les techniques de bases de l'analyse numérique, ainsi que l'algèbre linéaire. Les aspects numériques de l'ouvrage sont traités avec [R](https://www.r-project.org).

[**Modélisation des distributions de sinistres avec R**](https://vigou3.gitlab.io/modelisation-distributions-sinistres-avec-r/)
Ouvrage de référence exhaustif sur la modélisation statistique des distributions de sinistres en assurance avec [R](https://www.r-project.org) et le paquetage [**actuar**](https://cran.r-project.org/package=actuar)

[**Théorie de la crédibilité avec R**](https://vigou3.gitlab.io/theorie-credibilite-avec-r/)
Ouvrage de référence sur la théorie de la crédibilité couvrant la crédibilité de stabiblité, la tarification bayésienne et les modèles classiques de Bühlmann et de Bühlmann-Straub. Contient des exemples numériques réalisés avec [R](https://www.r-project.org) et le paquetage [**actuar**](https://cran.r-project.org/package=actuar).

[**Analyse statistique --- Exercices et solutions**](https://vigou3.gitlab.io/analyse-statistique-exercices-et-solutions/)
Recueil d'exercices pour un cours de statistique mathématique de premier ou de deuxième cycle universitaire.

## LaTeX

[**Rédaction avec LaTeX**](https://vigou3.gitlab.io/formation-latex-ul)
Document de référence offrant une introduction à l'utilisation du système de mise en page LaTeX. Distribué [via CTAN](https://ctan.org/pkg/formation-latex-ul).

[**bibliography**](https://gitlab.com/vigou3/bibliography)
Ma base de données bibliographique en format BIBTeX. Parce que la conserver sous gestion de versions est une bonne chose.


# Logiciel

## Distributions de GNU Emacs

[**Emacs modified for (macOS|Windows)**](https://emacs-modified.gitlab.io) 
Distributions de [GNU Emacs](https://www.gnu.org/software/emacs/) munies de quelques extensions particulièrement utiles pour les utilisateurs de LaTeX et de R, notamment [AUCTeX](https://www.gnu.org/software/auctex/) et [ESS](https://ess.r-project.org/). La version Windows est munie d'un assistant d'installation.

## Projet Roger

[**Roger l'omnicorrecteur**](https://roger-project.gitlab.io) 
Roger est un système de correction automatisée pour les travaux informatiques. Spécialisé dans la correction de code R, Roger peut être adapté à d'autres langages de programmation interprétés ou compilés grâce à sa polyvalence et à sa modularité.

## Paquetages R

[**actuar**](https://gitlab.com/vigou3/actuar) 
Le premier paquetage R dédié spécifiquement à la modélisation et aux calculs et actuariels. Distribué [via CRAN](https://cran.r-project.org/package=actuar).

[**expint**](https://gitlab.com/vigou3/expint)
Paquetage R dédié au calcul de l'intégrale exponentielle et de la fonction gamma incomplète. Distribué [via CRAN](https://cran.r-project.org/package=expint).

[**RweaveExtra**](https://gitlab.com/vigou3/RweaveExtra)
Petit paquetage R qui bonifie le système de programmation lettrée Sweave en proposant deux modules de traitement (*drivers*): `RweaveExtraLatex` et `RtangleExtra`. Ces modules ajoutent des options aux modules standards `RweaveLatex` et `Rtangle` pour ignorer des blocs de code lors de la phase *weave*, lors de la phase *tangle*, ou les deux. Une option permet aussi de spécifier l'extension du fichier de sortie lors de la phase *tangle*. Distribué [via CRAN](https://cran.r-project.org/package=RweaveExtra).

## Paquetages LaTeX

[**ulthese**](https://vigou3.gitlab.io/ulthese)
Classe LaTeX officielle pour les thèses et mémoires de l'[Université Laval](https://ulaval.ca). Distribué [via CTAN](https://ctan.org/pkg/ulthese).

[**francais-bst**](https://gitlab.com/vigou3/francais-bst)
Styles bibliographiques compatibles avec **natbib** permettant de composer des bibliographies conformes aux règles de présentation françaises. Distribué [via CTAN](https://ctan.org/pkg/francais-bst).

[**actuarialsymbol**](https://vigou3.gitlab.io/actuarialsymbol)
Paquetage de commandes spécialisées dans la composition de symboles de mathématiques actuarielles vie et de mathématiques financières. Parmi les caractéristiques de ces symboles, on retrouve le fait qu'il peuvent contenir des exposants et des indices de part et d'autre d'un symbole principal. Distribué [via CTAN](https://ctan.org/pkg/actuarialsymbol).

[**actuarialangle**](https://vigou3.gitlab.io/actuarialsymbol)
Paquetage de commandes spécialisées pour la composition du symbole d'«angle» et du symbole de statut conjoint dans les symboles actuariels. Distribué [via CTAN](https://ctan.org/pkg/actuarialangle).
 
Je suis aussi le créateur et mainteneur du paquetage [**cjs-rcs-article**](https://cjs-rcs.gitlab.io/fr) pour [*La revue canadienne de statistique*](https://ssc.ca/fr/publications/revue-canadienne-statistique).


# Électrons libres

[**Données ouvertes BIXI uniformes**](https://vigou3.gitlab.io/bixi-donnees-ouvertes-uniformes)
L'organisme à but non lucratif [BIXI Montréal](https://www.bixi.com/fr) publie les données ouvertes d'utilisation de son service de vélopartage. Cependant, le format de ces données change au fil du temps. Cela rend difficile, par exemple, de construire des outils logiciels exploitant ces données qui demeurent valides pour n'importe quelle année. Ce projet fournit les données ouvertes originales de BIXI dans un format uniforme d'une année à l'autre.

[**BitbucketAPI**](https://gitlab.com/vigou3/bitbucketapi)
Procédures d'interpréteur de commandes pour effectuer certaines tâches d'administration d'un référentiel Bitbucket à l'aide de l'API REST de Atlassian.


# Conférences et ateliers

[**Bernoulli, Bayes et la protection contre l'infortune**](https://gitlab.com/vigou3/bernoulli-bayes-et-vous)
Diapositives d'une conférence présentant les concepts de base de l'assurance et des sciences actuarielles.

[**IA et reconnaissance de caractères - Atelier d'introduction à la sciences des données**](https://vigou3.gitlab.io/atelier-science-donnees/)
Matériel d'une courte formation d'initiation à la science des données dans le contexte de la reconnaissance de caractères. Contient des images additionnelles de chiffres écrits à la main qu'il est possible de comparer au célèbre jeu de données [Optical Recognition of Handwritten Digits](https://archive.ics.uci.edu/ml/datasets/Optical+Recognition+of+Handwritten+Digits). Contient également une [application Shiny](https://vigou3.shinyapps.io/atelier-science-donnees).

[**Gérer ses documents efficacement avec la programmation lettrée - R à Québec 2019**](https://gitlab.com/vigou3/raquebec-programmation-lettree)
Diapositives d'une conférence donnée à [R à Québec 2019](http://raquebec.ulaval.ca/2019/) qui démontre comment l'étape *tangle* de la programmation lettrée, lorsqu'utilisée conjointement avec la mieux connue étape *weave*, permet de gérer plus efficacement ses documents.

[**Programmer pour collaborer: utilisation et conception d'une interface de programmation applicative (API) - R à Montréal 2018**](https://gitlab.com/vigou3/ramontreal-expint)
Diapositives d'une conférence donnéee à [R à Montréal 2018](http://rmontreal2018.ca/) qui explique comment concevoir une API pour donner accès au code C de son paquetage R et comment importer les fonctionnalités d'un autre paquetage.

[**You (S)wove? Well (S)tangle now!**](https://gitlab.com/vigou3/tug-2024-you-swove-well-stangle-now)
Diapositives (en anglais) d'une conférence donnée à [TUG 2024](https://tug.org/tug2024/), similaire à celle de R à Québec 2019, ci-dessus, mais dans une présentation revampée et qui ne nécessite pas de consulter du code source externe.

[**A journey through the design of (yet another) journal class**](https://gitlab.com/vigou3/tug-2024-cjs-rcs-article)
Diapositives (en anglais) d'une conférence donnée à [TUG 2024](https://tug.org/tug2024/) portant sur la conception du paquetage [**cjs-rcs-article**](https://cjs-rcs.gitlab.io) qui fournit la classe LaTeX et les styles de bibliographie pour les articles de [*La revue canadienne de statistique*](https://ssc.ca/fr/publications/revue-canadienne-statistique).

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode -1) -->
<!-- eval: (visual-line-mode) -->
<!-- End: -->
 
